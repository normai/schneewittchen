/**
 * file : 20220129�1411 sneewittchen/sneewitlib/utilitywitch.h
 * sum : An utility functions collection as header-only library
 * license : BSD 3-Clause
 * authors :
 * copyright : 2022 Norbert C. Maier
 */

#ifndef WITCH_WIN_UTILS_H
#define WITCH_WIN_UTILS_H

#include <string>
#include <windows.h>                                   // e.g. MessageBox
using namespace std;

 /**
  *  function : 20220129�1421
  *  summary : Convert std::string to LPCWSTR
  *  remember : This func is made from former seq 20220122�1537 after ref 20220122�1535
  *  chain : func 20220122�1421 'Convert C-string to CPP-string'
  *  ref : stackoverflow.com/questions/27220/how-to-convert-stdstring-to-lpcwstr-in-c-unicode [ref 20220122�1535]
  *  ref : stackoverflow.com/questions/2230758/what-does-lpcwstr-stand-for-and-how-should-it-be-handled-with [ref 20220129�1432]
  *  ref : stackoverflow.com/questions/4379696/returning-lpcwstr-from-a-function [ref 20220129�1433] Answer 2020-Dec-07 by Abyx
  */
////const LPCWSTR conv_cppstring_to_lpcwstr(string sIn)
const wstring conv_cppstring_to_lpcwstr(string sIn)
{
   wstring sTmp = wstring(sIn.begin(), sIn.end());
   LPCWSTR sw = sTmp.c_str();
   return sw;
}

#endif // WITCH_WIN_UTILS_H
