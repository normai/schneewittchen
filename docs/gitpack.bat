rem 20220123`0821
rem https://stackoverflow.com/questions/35410336/how-to-compact-local-git-repo [ref 20220123`0812]

# Expire unreachable content and pack repo most compact
git gc --aggressive --prune=now

# Clear the reflog as well
git reflog expire --expire=now --all
