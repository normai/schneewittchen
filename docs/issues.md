﻿
   ****************
   Issues and Todos
   ****************

   issue 20220129°1441 'How to print long dash?'
   matter : Having a std::string with a long dash, then converting it
      into a LPCWSTR for the title line results in a ~Japanese char.
   location : sneewittwin/main.cpp seq 20220126°1811
   status :

   issue 20220129°1051 'GitLab FlawFinder complains'
   see : ref 20220129°1042 FlawFinder warnings e.g. "Error: encoding
      error in ./sneewitwin/main.cpp 'utf-8' codec can't decode
      byte 0xb0 in position 16: invalid start byte"
   finding : The *.cpp and *.h files are ANSI-encoded, the *.md
      and *.txt files are (already) UTF-8-with-BOM encoded.
   finding : The one *.rc file 20220122°1327 'sneewittwin.rc'
      is UCS-2-Little-Endian encoded. I leave it as is
   fix : Make all *.CPP, *.H files UTF-8-with-BOM encoded.
   finding :
   status :
   ܀

   issue 20220111°2055 'WinGui fails with lib'
   matter : While trying to activat the library, the WittGui compiles
      but crashes then even before entry into the main function.
   chain : 20220111o2054.vs--wittgui--crash-before-main.png
   chain : Flag 20220111°2056 LOAD_LIB/LOAD_LIB_NO
   note : What was done to activate the library
      • In project WittGui add reference to project WittLib
      • In WittGui properties, add AdditionalIncludeDirectories [ss 20220111°2025/°2026]
      • Add #include "wittlib.h"
   status : WinGui runs only if LOAD_LIB is not set
   ܀

   todo 20220111°2021 'condense'
   location : wittcon.cpp
   ܀

   todo 20220111°1931 'library not found'
   do : Gracefully handle the situation when the library is missing.
   status : Open
   ܀

   ———————————————————————
   [file 20220108°0846] ܀Ω
