﻿### Some Project Details

Project Sneewittchen shall demonstrate a strict separation of
 functionality and user interface.

Here is a diagram to illustrate the outline of this demo project.

<a href="./stack/20220108o1314.schneewittchen-komponenten.v0.uxf.png"><img src="./stack/20220108o1314.schneewittchen-komponenten.v0.uxf.png" width="600" height="520" alt="Components overview"></a>






<sup><sub>*[file 20220108°0845]* ܀Ω</sub></sup>
