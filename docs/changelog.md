﻿
   version 20220129°1451 v0.1.3 — UTF-8
   • Convert all CPP/H files from ANSI to UTF-8-with-BOM
   • First experiences with WinAPI strings like LPCWSTR
   • Output delegates work with SneewitWin
   • Still many warnings

   func 20220129°1021 Dynamically create TextBox

   version 20220126°1911 v0.1.2 en route
   • Outsource code from main.cpp to apicalls.cpp/libcalls.cpp
   • Delete inactive projects z.wittgui and z.d03_menu_one

   chg 20220125°0746 rename repo
   • Rename repository plus URL from Schneewittchen to Sneewittchen

   log 20220123°0911 v0.1.1 — Proof-of-concept
   • Projects renamed from WittCon to SneewittCon etc.
   • Project SneewittWin works with library proof-of-concept
   • For the not-working-library-access see project 'z.wittgui'

   log 20220123°1251 Create GitLab repository

   version v0.1.0 — 20220111°1921 — Only CPP
   • Add button 20220108°1711 'More'
   • Convert WittGui from C to C++

   version v0.0.1 — 20220108°1511 — Initial version
   • Library outputs via function object to console
   • Library input not yet tested
   • GUI unchanged as it came from WinApiTut

   ———————————————————————
   [file 20220108°0843] ܀Ω
