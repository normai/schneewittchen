﻿// file : 20220126°1721 sneewittchen/sneewittwin/apicalls.cpp
// sum : This module shall collect the functions which call the WinAPI
// encoding : UTF-8-with-BOM

#include "apicalls.h"

// func 20220122°1346
// Message handler for about box
INT_PTR CALLBACK GoAbout(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
   UNREFERENCED_PARAMETER(lParam);
   switch (message)
   {
   case WM_INITDIALOG:
      return (INT_PTR)TRUE;

   case WM_COMMAND:
      if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
      {
         EndDialog(hDlg, LOWORD(wParam));
         return (INT_PTR)TRUE;
      }
      break;
   }
   return (INT_PTR)FALSE;
}
