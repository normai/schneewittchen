﻿// note 20220208°0741 : The curious code blocks inside switch can possibly refactured away, see //*//


// file : 20220122°1321 sneewittchen/sneewittwin/main.cpp
// summary : Try accessing the DLL from a VS2022 generated 'Windows Desktop Application'
// encoding : UTF-8-with-BOM

#include <codecvt>                             // Deprecated with C++17
#include <locale>                              //
#include "apicalls.h"
#include "framework.h"                         // Possibly eliminate?
#include "libcalls.h"
#include "Resource.h"
#include "sneewittlib.h"                       // [sprint 20220122°1511`03]
#include "witch_win_utils.h"

#define SNEEWITT_WIN_VERSION L"0.1.3"
#define MAX_LOADSTRING 100                     // Not shielded against overflow?! [issue 20220126°1751]

// Global Variables:
HINSTANCE hInst;                               // current instance
WCHAR szTitle[MAX_LOADSTRING];                 // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];           // the main window class name

// Forward declarations of functions included in this code module:
ATOM MyRegisterClass(HINSTANCE hInstance);
HWND InitInstance(HINSTANCE, int);             // [chg 20220129°1025`01]
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND CreateTextBox ( CONST INT iX                              //
                    , CONST INT iY                             //
                     , CONST UINT uWidth                       //
                      , CONST UINT uHeight                     //
                       , HWND hWnd                             //
                        , CONST UINT uId                       //
                         , HINSTANCE hInstance                 //
                          );

// func 20220122°1341
int APIENTRY wWinMain ( _In_ HINSTANCE hInstance
                       , _In_opt_ HINSTANCE hPrevInstance
                        , _In_ LPWSTR lpCmdLine
                         , _In_ int nCmdShow
                          )
{
   UNREFERENCED_PARAMETER(hPrevInstance);
   UNREFERENCED_PARAMETER(lpCmdLine);

   // TODO: Place code here. //// Line left from VS2022 project template [screenshot 20220108°1312 'Create new DLL project']

   // Initialize global strings
   LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
   LoadStringW(hInstance, IDC_SNEEWITTWIN, szWindowClass, MAX_LOADSTRING);
   MyRegisterClass(hInstance);

   // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   // Build version string [seq 20220126°1811]
   // ref : stackoverflow.com/questions/842268/need-help-appending-one-wchar-t-to-another-c
   // ref : docs.microsoft.com/en-us/cpp/c-runtime-library/reference/strcpy-s-wcscpy-s-mbscpy-s?view=msvc-170
   // ref : stackoverflow.com/questions/2573834/c-convert-string-or-char-to-wstring-or-wchar-t [ref 20220129°1142]
   // ref : ideone.com/KA1oty [ref 20220129°1143] Sample by Johann Gerell
   ////LPWSTR sTitleBuffer;
   ////WCHAR lpsLibVer = (WCHAR) sLibVer;
   ////wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
   ////wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;
   ////string narrow = converter.to_bytes(wide_utf16_source_string);
   ////string narrow = converter.to_bytes(wide_utf16_source_string);
   ////WCHAR szTitle2 = wcscat_s(szTitle

   ////wstring sDelim11 = conv_cppstring_to_lpcwstr(" — ");    // ⚡ [issue 20220129°1441 'How to print long dash?']
   wstring sDelim11 = conv_cppstring_to_lpcwstr(" \x0097 ");   // ⚡ [issue 20220129°1441 'How to print long dash?'] Same as "—"
   LPCWSTR sDelim12 = sDelim11.c_str();                        //
   errno_t err13 = wcscat_s(szTitle, sDelim12);

   wchar_t sBufProgVer[99] = SNEEWITT_WIN_VERSION;
   errno_t err1 = wcscat_s ( szTitle                           // *strDestination; (&strDestination)[size]
                            , sBufProgVer                      // *strSource
                             );

   ////string sLibVer = sneewittchen_version();
   ////wchar_t sBufLibVer[99] = &sLibVer;
   ////const LPCWSTR sBufLibVer1 = conv_cppstring_to_lpcwstr(sLibVer);
   ////auto sBufLibVer2 = conv_cppstring_to_lpcwstr(sLibVer);
   ////wstring sBufLibVer2 = conv_cppstring_to_lpcwstr(sLibVer);
   ////wchar_t sBufLibVer3[99] = L"x";
   ////LPCWSTR sBufLibVer3 = sBufLibVer2.c_str();

   wstring sDelim21 = conv_cppstring_to_lpcwstr("/");          // Why can those two lines not be ..
   LPCWSTR sDelim22 = sDelim21.c_str();                        // .. chained int one line?!
   errno_t err23 = wcscat_s(szTitle, sDelim22);

   string sLibVer = sneewittchen_version();
   wstring sBufLibVer3 = conv_cppstring_to_lpcwstr(sLibVer);   // Why can those two lines not be ..
   LPCWSTR sBufLibVer4 = sBufLibVer3.c_str();                  // .. chained int one line?!
   errno_t err3 = wcscat_s(szTitle, sBufLibVer4);
   // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~


   // Perform application initialization
   HWND hWnd = InitInstance(hInstance, nCmdShow);      // [chg 20220129°1025`02]
   if (hWnd == nullptr) {
      return FALSE;
   }

   GoCanaries();

   HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SNEEWITTWIN));

   // Put EditBox on window
   HWND hWndTextBox = CreateTextBox ( 22                     // INT iX //
                                     , 22                    // INT iY //
                                      , 333U            // UINT uWidth //
                                       , 200U          // UINT uHeight //
                                        , hWnd                         //
                                         , 123U      // CONST UINT uId //
                                          , hInstance                  //
                                           );

   // Main message loop [seq 20220122°1342]
   MSG msg;
   while (GetMessage(&msg, nullptr, 0, 0))
   {
      if (! TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
      {
         TranslateMessage(&msg);
         DispatchMessage(&msg);
      }
   }

   // Exit application
   return (int) msg.wParam;
}

// func 20220122°1343
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
   WNDCLASSEXW wcex;

   wcex.cbSize = sizeof(WNDCLASSEX);

   wcex.style          = CS_HREDRAW | CS_VREDRAW;
   wcex.lpfnWndProc    = WndProc;
   wcex.cbClsExtra     = 0;
   wcex.cbWndExtra     = 0;
   wcex.hInstance      = hInstance;
   wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SNEEWITTWIN));
   wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
   wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
   wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_SNEEWITTWIN);
   wcex.lpszClassName  = szWindowClass;
   wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

   return RegisterClassExW(&wcex);
}

//  func 20220122°1344
//  FUNCTION: InitInstance(HINSTANCE, int)
//
//  PURPOSE: Saves instance handle and creates main window
//
//  COMMENTS:
//
//       In this function, we save the instance handle in a global variable and
//       create and display the main program window.
//
////BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)   // [chg 20220129°1025`03]
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW ( szWindowClass           //
                              , szTitle                //
                               , WS_OVERLAPPEDWINDOW   //
                                , CW_USEDEFAULT        //
                                 , 0                   //
                                  , CW_USEDEFAULT      //
                                   , 0                 //
                                    , nullptr          //
                                     , nullptr         //
                                      , hInstance      //
                                       , nullptr       //
                                        );

   if (! hWnd)
   {
      return nullptr;                                 // [chg 20220129°1025`03]
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return hWnd;                                        // [chg 20220129°1025`04]
}

//  func 20220122°1345
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
   switch (message)
   {
   case WM_COMMAND :
      { //*//
         int wmId = LOWORD(wParam);
         // Parse the menu selections:
         switch (wmId)
         {
         case IDM_ABOUT:
             DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, GoAbout);
             break;
         case IDM_EXIT:
             DestroyWindow(hWnd);
             break;
         default:
             return DefWindowProc(hWnd, message, wParam, lParam);
             //*//break;
         }
      }         //*//
    break;      //*//
   case WM_PAINT:
      {
          PAINTSTRUCT ps;
          HDC hdc = BeginPaint(hWnd, &ps);
          // TODO: Add any drawing code that uses hdc here...
          EndPaint(hWnd, &ps);
      }
      break;
   case WM_DESTROY:
      PostQuitMessage(0);
      break;
   default:
      return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}


/**
 * func 20220129°0931
 * sum : Dynamically create a button
 * ref : docs.microsoft.com/en-us/windows/win32/controls/create-a-button
 * ref : docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-createwindowexa
 *
 */
/*
HWND hwndButton = CreateWindow(
   L"BUTTON",  // Predefined class; Unicode assumed 
   L"OK",      // Button text 
   WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
   10,         // x position 
   10,         // y position 
   100,        // Button width
   100,        // Button height
   m_hwnd,     // Parent window
   NULL,       // No menu.
   (HINSTANCE)GetWindowLongPtr(m_hwnd, GWLP_HINSTANCE),
   NULL);      // Pointer not needed.
*/


/**
 * func 20220129°1021 Dynamically create TextBox
 * ref : stackoverflow.com/questions/3773916/what-is-the-simplest-way-to-create-edit-box-in-c [ref 20220129°1012]
 * note : This function implementation brings chg 20220129°1025
 */
HWND CreateTextBox ( CONST INT iX                              //
                    , CONST INT iY                             //
                     , CONST UINT uWidth                       //
                      , CONST UINT uHeight                     //
                       , HWND hWnd                             //
                        , CONST UINT uId                       //
                         , HINSTANCE hInstance                 //
                          )
{
   HWND hWndTextBox = CreateWindowEx ( WS_EX_CLIENTEDGE        //
                                      , TEXT ("Edit")          //
                                       , NULL                  //
                                        , WS_CHILD             //
                                         , iX                  //
                                         , iY                  //
                                         , (signed) uWidth     //
                                         , (signed) uHeight    //
                                         , hWnd                //
                                          , (HMENU) uId        //
                                           , hInstance         //
                                            , NULL             //
                                             );
   SetBkColor ( GetDC(hWndTextBox)                             //
               , RGB(255, 255, 255)                            //
                );

   // Supplemented empirically after sequence in func 20220122°1344 above
   ShowWindow(hWndTextBox, 10); // nCmdShow);
   UpdateWindow(hWndTextBox);

   return hWndTextBox;
}
