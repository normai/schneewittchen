﻿// file : 20220126°1733 sneewittchen/sneewittwin/libcalls.h
// encoding : UTF-8-with-BOM

#ifndef LIBCALLS_H
#define LIBCALLS_H

#include <string>
#include "framework.h"                                      // [sprint 20220122°1511`03]
#include "main.h"                                           // [sprint 20220122°1511`03]
#include "sneewittlib.h"
using namespace std;

void GoCanaries();
/*
void sneewittchen_canary1();
void sneewittchen_canary2(string s);
string sneewittchen_canary3(string s);
*/

#endif // LIBCALLS_H
